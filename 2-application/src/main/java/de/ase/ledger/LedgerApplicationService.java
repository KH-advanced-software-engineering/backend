package de.ase.ledger;

import de.ase.transaction.Transaction;
import de.ase.types.Balance;
import de.ase.types.Unit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LedgerApplicationService {
    private final LedgerRepository ledgerRepository;

    @Autowired
    private LedgerApplicationService(LedgerRepository ledgerRepository) {
        this.ledgerRepository = ledgerRepository;
    }

    public void createLedger(Ledger ledger) throws IllegalArgumentException {
        if (ledger.getName() == null) {
            throw new IllegalArgumentException("A new ledger needs a name.");
        }
        if (ledger.getBalance() == null) {
            ledger.setBalance(new Balance(0, Unit.EURO));
        }
        ledgerRepository.save(ledger);
    }

    public Ledger getLedgerById(Long id) throws IllegalArgumentException {
        if (ledgerRepository.getById(id) == null) {
            throw new IllegalArgumentException("No ledger with this id exists.");
        }
        return ledgerRepository.getById(id);
    }

    public void updateLedger(Ledger ledger, String name, Balance balance, Transaction transaction) {
        Ledger ledgerWithId = ledgerRepository.getById(ledger.getId());
        if (ledgerWithId == null) {
            throw new IllegalArgumentException("No ledger with this id exists.");
        }
        if (name != null) {
            ledgerWithId.setName(name);
        }
        if (balance != null) {
            ledgerWithId.setBalance(balance);
        }
        if (transaction != null) {
            ledgerWithId.getTransactions().add(transaction);
        }
        ledgerRepository.save(ledgerWithId);
    }

    public void deleteLedger(Ledger ledger) {
        if (ledger == null || ledgerRepository.getById(ledger.getId()) == null) {
            throw new IllegalArgumentException("No ledger with this id exists.");
        }
        ledgerRepository.delete(ledger);
    }
}
