package de.ase.category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryApplicationService {
    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryApplicationService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public void createCategory(Category category) throws IllegalArgumentException {
        if (category == null || category.getName() == null) {
            throw new IllegalArgumentException("A new Category needs a name.");
        }
        if (categoryRepository.getByName(category.getName()) != null) {
            throw new IllegalArgumentException("A Category with this name already exists.");
        }
        categoryRepository.save(category);
    }

    public Category getCategoryByName(String name) throws IllegalArgumentException {
        if (categoryRepository.getByName(name) == null) {
            throw new IllegalArgumentException("No Category with this name exists.");
        }
        return categoryRepository.getByName(name);
    }

    public List<Category> getAllCategories() {
        return categoryRepository.findAll();
    }

    public void updateCategory(Category category, String name, String description) {
        Category categoryWithName = categoryRepository.getByName(category.getName());
        if (categoryWithName == null) {
            throw new IllegalArgumentException("No Category with this name exists.");
        } else {
            categoryWithName.setName(name);
            categoryWithName.setDescription(description);
            categoryRepository.save(category);
        }
    }

    public void deleteCategory(Category category) {
        if (category == null || this.categoryRepository.getByName(category.getName()) == null) {
            throw new IllegalArgumentException("No Category with this name exists.");
        }
        categoryRepository.delete(category);
    }
}
