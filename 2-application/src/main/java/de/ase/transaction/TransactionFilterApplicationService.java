package de.ase.transaction;

import de.ase.ledger.Ledger;
import de.ase.repeatableEvent.RepeatableEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TransactionFilterApplicationService {
    private TransactionRepository transactionRepository;

    @Autowired
    private TransactionFilterApplicationService(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    public boolean isTransactionADuplicate(Transaction transaction, Ledger ledger) {
        if (transaction.getLedger() != ledger) {
            return false;
        }
        return !getAllTransactionsForLedger(ledger).stream()
                .filter(t -> compareTransactionData(transaction, t)).collect(Collectors.toList()).isEmpty();
    }

    private boolean compareTransactionData(Transaction transaction, Transaction t) {
        if (transaction == null || t == null) {
            return false;
        }
        RepeatableEvent rEvent = t.getRepeatableEvent();
        RepeatableEvent repeatableEvent = transaction.getRepeatableEvent();
        if (t.getAmount().getAmount() == transaction.getAmount().getAmount()
                && t.getAmount().getUnit() == transaction.getAmount().getUnit()
                && t.getType() == transaction.getType()
                && t.getCategory().getName() == transaction.getCategory().getName()
                && rEvent.getStartDate().isEqual(repeatableEvent.getStartDate())
                && rEvent.getEndDate().isEqual(repeatableEvent.getEndDate())
                && rEvent.getInterval() == repeatableEvent.getInterval()) {
            return true;
        }
        return false;
    }

    public List<Transaction> getTransactionsInIntervalByType(LocalDate start, LocalDate end, Ledger ledger,
                                                             TransactionType type) {
        return getAllTransactionsInInterval(start, end, ledger).stream()
                .filter(transaction -> transaction.getType() == type).collect(Collectors.toList());
    }

    public List<Transaction> getAllTransactionsInInterval(LocalDate start, LocalDate end, Ledger ledger) {
        return getAllTransactionsForLedger(ledger).stream()
                .filter(transaction -> isInInterval(start, end, transaction)).collect(Collectors.toList());
    }

    private boolean isInInterval(LocalDate start, LocalDate end, Transaction transaction) {
        LocalDate startDate = transaction.getRepeatableEvent().getStartDate();
        LocalDate endDate = transaction.getRepeatableEvent()
                .getEndDate();
        if (startDate == start || endDate == end || (startDate.isAfter(start) && startDate.isBefore(end)) || (endDate
                .isAfter(start) && endDate.isBefore(end)) || (startDate
                .isBefore(start) && endDate.isAfter(start))) {
            return true;
        }
        return false;
    }

    private List<Transaction> getAllTransactionsForLedger(Ledger ledger) {
        return transactionRepository.findAllByLedger(ledger);
    }
}