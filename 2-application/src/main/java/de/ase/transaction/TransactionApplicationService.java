package de.ase.transaction;

import de.ase.category.Category;
import de.ase.ledger.Ledger;
import de.ase.ledger.LedgerRepository;
import de.ase.repeatableEvent.RepeatableEvent;
import de.ase.balanceCalculation.LedgerBalanceCalculationStrategy;
import de.ase.types.Amount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

@Service
public class TransactionApplicationService {
    private TransactionRepository transactionRepository;
    private LedgerRepository ledgerRepository;

    @Autowired
    private TransactionApplicationService(TransactionRepository transactionRepository,
                                          LedgerRepository ledgerRepository) {
        this.transactionRepository = transactionRepository;
        this.ledgerRepository = ledgerRepository;
    }

    public void createTransaction(Transaction transaction, LedgerBalanceCalculationStrategy strategy) throws
            IllegalArgumentException, ParseException {
        if (transaction == null || transaction.getCategory() == null) {
            throw new IllegalArgumentException("The transaction is not valid.");
        }
        validateRepeatableEvent(transaction.getRepeatableEvent());
        if (transactionRepository.existsById(transaction.getId())) {
            throw new IllegalArgumentException("A transaction with this id already exists.");
        }
        if (transaction.getAmount() != null && transaction.getType() != null && transaction.getLedger() != null) {
            Ledger ledger = transaction.getLedger();
            adjustBalanceOfLedger(transaction, ledger, strategy);
        } else {
            throw new IllegalArgumentException("A new transaction needs a amount and a transaction type.");
        }
        transactionRepository.save(transaction);
    }

    private void adjustBalanceOfLedger(Transaction transaction, Ledger ledger,
                                       LedgerBalanceCalculationStrategy strategy) {
        strategy.calculateNewBalanceForLeger(ledger, transaction.getAmount());
        ledger.getTransactions().add(transaction);
        ledgerRepository.save(ledger);
    }

    private void validateRepeatableEvent(RepeatableEvent event) throws ParseException {
        LocalDate startDate = event.getStartDate();
        LocalDate endDate = event.getEndDate();
        if (event == null || startDate == null) {
            throw new IllegalArgumentException("A new transaction needs a start date.");
        } else if (endDate != null) {
            validateInterval(event, startDate, endDate);
        }
    }

    private void validateInterval(RepeatableEvent event, LocalDate startDate, LocalDate endDate) throws ParseException {
        if (startDate.isAfter(endDate)) {
            throw new IllegalArgumentException("The starting date should be before the end date");
        } else if (event
                .getInterval() > 0) {
            long diff = getDiffBetweenToDates(startDate, endDate);
            if (event.getInterval() > diff) {
                throw new IllegalArgumentException(
                        "The interval should be greater than the difference between start date and end date");
            }
        } else {
            throw new IllegalArgumentException("If an end date exists the interval should be greater than 0");
        }
    }

    private long getDiffBetweenToDates(LocalDate startDate, LocalDate endDate) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
        Date firstDate = sdf.parse(startDate.toString());
        Date secondDate = sdf.parse(endDate.toString());

        long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
        return TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }

    public Transaction getTransactionById(Long id) throws IllegalArgumentException {
        if (transactionRepository.existsById(id)) {
            throw new IllegalArgumentException("No transaction with this name exists.");
        }
        return transactionRepository.getById(id);
    }

    public List<Transaction> getAllTransactionsForLedger(Ledger ledger) {
        return transactionRepository.findAllByLedger(ledger);
    }

    public void updateTransaction(Long id, Amount amount, Category category, TransactionType type,
                                  RepeatableEvent repeatableEvent) {
        Transaction transaction = transactionRepository.getById(id);
        if (transaction == null) {
            throw new IllegalArgumentException("No transaction with this id exists.");
        }
        if (category != null) {
            transaction.setCategory(category);
        }
        if (type != null) {
            transaction.setType(type);
        }
        if (repeatableEvent != null) {
            transaction.setRepeatableEvent(repeatableEvent);
        }
        if (amount != null) {
            transaction.setAmount(amount);
        }
        transactionRepository.save(transaction);
    }

    public void deleteTransaction(Long id) {
        if (transactionRepository.getById(id) == null) {
            throw new IllegalArgumentException("No transaction with this name exists.");
        }
        transactionRepository.deleteById(id);
    }
}