package de.ase.category;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CategoryApplicationServiceTest {

    @Mock
    private CategoryRepository categoryRepository;

    private CategoryApplicationService categoryApplicationService;
    private Category category;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        categoryApplicationService = new CategoryApplicationService(categoryRepository);
        category = new Category(17L, "name", "description");
    }

    @Test
    void testCreateCategory() {
        categoryApplicationService.createCategory(category);
        Mockito.verify(categoryRepository, Mockito.times(1)).save(category);
        Mockito.verify(categoryRepository, Mockito.times(0)).delete(category);
    }

    @Test
    void testCreateCategoryWithNull() {
        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> categoryApplicationService.createCategory(null),
                "Expected createCategory to throw, but it didn't"
        );
        assertTrue(thrown.getMessage().contains("A new Category needs a name."));
        Mockito.verify(categoryRepository, Mockito.times(0)).save(category);
    }

    @Test
    void testCreateCategoryDuplicate() {
        categoryApplicationService.createCategory(category);
        Mockito.when(categoryRepository.getByName(category.getName())).thenReturn(category);
        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> categoryApplicationService.createCategory(category),
                "Expected createCategory to throw, but it didn't"
        );
        assertTrue(thrown.getMessage().contains("A Category with this name already exists."));
        Mockito.verify(categoryRepository, Mockito.times(1)).save(category);
    }

    @Test
    void testGetCategoryByName() {
        String name = category.getName();
        Mockito.when(categoryRepository.getByName(name)).thenReturn(category);
        categoryApplicationService.getCategoryByName(name);
        Mockito.verify(categoryRepository, Mockito.times(2)).getByName(name);
        Mockito.verify(categoryRepository, Mockito.times(0)).delete(category);
    }

    @Test
    void testGetCategoryByNameWithEmptyString() {
        String name = category.getName();
        Mockito.when(categoryRepository.getByName("")).thenReturn(null);
        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> categoryApplicationService.getCategoryByName(""),
                "Expected createCategory to throw, but it didn't"
        );
        assertTrue(thrown.getMessage().contains("No Category with this name exists."));
        Mockito.verify(categoryRepository, Mockito.times(1)).getByName("");
        Mockito.verify(categoryRepository, Mockito.times(0)).delete(category);
    }

    @Test
    void getAllCategories() {
        categoryApplicationService.getAllCategories();
        Mockito.verify(categoryRepository, Mockito.times(1)).findAll();
        Mockito.verify(categoryRepository, Mockito.times(0)).delete(category);
    }

    @Test
    void updateCategory() {
        String name = category.getName();
        Mockito.when(categoryRepository.getByName(name)).thenReturn(category);
        categoryApplicationService.updateCategory(category, name, category.getDescription());
        Mockito.verify(categoryRepository, Mockito.times(1)).getByName(name);
        Mockito.verify(categoryRepository, Mockito.times(1)).save(category);
    }

    @Test
    void updateCategoryCategoryWhichDoesNotExists() {
        String name = category.getName();
        Mockito.when(categoryRepository.getByName(name)).thenReturn(null);
        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> categoryApplicationService.updateCategory(category, name, category.getDescription()),
                "Expected createCategory to throw, but it didn't"
        );
        assertTrue(thrown.getMessage().contains("No Category with this name exists."));
        Mockito.verify(categoryRepository, Mockito.times(1)).getByName(name);
        Mockito.verify(categoryRepository, Mockito.times(0)).save(category);
    }

    @Test
    void deleteCategory() {
        String name = category.getName();
        Mockito.when(categoryRepository.getByName(name)).thenReturn(category);
        categoryApplicationService.deleteCategory(category);
        Mockito.verify(categoryRepository, Mockito.times(1)).getByName(name);
        Mockito.verify(categoryRepository, Mockito.times(1)).delete(category);
    }

    @Test
    void deleteCategoryThatDoesNotExist() {
        String name = category.getName();
        Mockito.when(categoryRepository.getByName(name)).thenReturn(null);
        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> categoryApplicationService.deleteCategory(category),
                "Expected createCategory to throw, but it didn't"
        );
        assertTrue(thrown.getMessage().contains("No Category with this name exists."));
        Mockito.verify(categoryRepository, Mockito.times(1)).getByName(name);
        Mockito.verify(categoryRepository, Mockito.times(0)).delete(category);
    }
}