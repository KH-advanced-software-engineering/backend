# Financial management  

## About The Project

Project for the course Advanced Software Engineering at the DHBW Karlsruhe.  Content of the course is to learn principles about software programming e.g. DDD, SOLID and Clean Architecture.


### Built With

* [Spring Boot](https://spring.io/projects/spring-boot)


## Getting Started

### Prerequisites

- IDE, which supports maven projects 

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/KH-advanced-software-engineering/backend.git
   ```

2. Import the Folder 'ase' as a project in your IDE
3. Download needed dependencies with
   ```sh
   mvn install
   ```
4. Start the backend by executing 'Application.java'

### Tests

Run all test
```sh
   mvn test
   ```
<!-- USAGE EXAMPLES -->
## Usage

- [Swagger](http://localhost:8080/swagger-ui/index.html)
- [H2Console](http://localhost:8080/h2-ui/login.jsp)




