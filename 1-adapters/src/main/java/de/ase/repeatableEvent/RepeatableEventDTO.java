package de.ase.repeatableEvent;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import de.ase.transaction.Transaction;

import javax.annotation.Generated;
import java.time.LocalDate;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "startDate",
        "endDate",
        "interval",
        "transaction"
})
@Generated("jsonschema2pojo")
public class RepeatableEventDTO {
    @JsonProperty("id")
    private Long id;
    @JsonProperty("startDate")
    @JsonFormat(pattern = "MM/dd/yyyy")
    private LocalDate startDate;
    @JsonProperty("endDate")
    @JsonFormat(pattern = "MM/dd/yyyy")
    private LocalDate endDate;
    @JsonProperty("interval")
    private int interval;
    @JsonProperty("transaction")
    private Transaction transaction;

    public RepeatableEventDTO() {
    }

    public RepeatableEventDTO(Long id, LocalDate startDate, LocalDate endDate, int interval, Transaction transaction) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.interval = interval;
        this.transaction = transaction;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("startDate")
    public LocalDate getStartDate() {
        return startDate;
    }

    @JsonProperty("endDate")
    public LocalDate getEndDate() {
        return endDate;
    }

    @JsonProperty("interval")
    public int getInterval() {
        return interval;
    }

    @JsonProperty("transaction")
    public Transaction getTransaction() {
        return transaction;
    }
}
