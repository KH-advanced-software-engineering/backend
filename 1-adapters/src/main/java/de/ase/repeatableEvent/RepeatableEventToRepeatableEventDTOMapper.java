package de.ase.repeatableEvent;

import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class RepeatableEventToRepeatableEventDTOMapper implements Function<RepeatableEvent, RepeatableEventDTO> {
    @Override
    public RepeatableEventDTO apply(RepeatableEvent repeatableEvent) {
        return map(repeatableEvent);
    }

    private RepeatableEventDTO map(RepeatableEvent repeatableEvent) {
        return new RepeatableEventDTO(repeatableEvent.getId(), repeatableEvent.getStartDate(),
                repeatableEvent.getEndDate(), repeatableEvent.getInterval(), repeatableEvent.getTransaction());
    }
}
