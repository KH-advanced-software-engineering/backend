package de.ase.repeatableEvent;

import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class RepeatableEventDTOToRepeatableEventMapper implements Function<RepeatableEventDTO, RepeatableEvent> {
    @Override
    public RepeatableEvent apply(RepeatableEventDTO repeatableEventDTO) {
        return map(repeatableEventDTO);
    }

    private RepeatableEvent map(RepeatableEventDTO repeatableEventDTO) {
        if (repeatableEventDTO == null) {
            return null;
        }
        return new RepeatableEvent(repeatableEventDTO.getStartDate(), repeatableEventDTO.getEndDate(),
                repeatableEventDTO.getInterval());
    }
}
