package de.ase.amount;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import de.ase.transaction.Transaction;
import de.ase.types.Unit;

import javax.annotation.Generated;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "amount",
        "unit",
        "transaction"
})
@Generated("jsonschema2pojo")
public class AmountDTO {
    @JsonProperty("id")
    private Long id;
    @JsonProperty("amount")
    private int amount;
    @JsonProperty("unit")
    private Unit unit;
    @JsonProperty("transaction")
    private Transaction transaction;

    public AmountDTO(Long id, int amount, Unit unit, Transaction transaction) {
        this.id = id;
        this.amount = amount;
        this.unit = unit;
        this.transaction = transaction;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("amount")
    public int getAmount() {
        return amount;
    }

    @JsonProperty("unit")
    public Unit getUnit() {
        return unit;
    }

    @JsonProperty("transaction")
    public Transaction getTransaction() {
        return transaction;
    }
}
