package de.ase.amount;

import de.ase.types.Amount;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class AmountDTOToAmountMapper implements Function<AmountDTO, Amount> {
    @Override
    public Amount apply(AmountDTO amountDTO) {
        return map(amountDTO);
    }

    private Amount map(AmountDTO amountDTO) {
        if (amountDTO == null) {
            return null;
        }
        return new Amount(amountDTO.getAmount(), amountDTO.getUnit());
    }
}
