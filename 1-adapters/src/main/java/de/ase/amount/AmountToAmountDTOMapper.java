package de.ase.amount;

import de.ase.types.Amount;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class AmountToAmountDTOMapper implements Function<Amount, AmountDTO> {
    @Override
    public AmountDTO apply(Amount amount) {
        return map(amount);
    }

    private AmountDTO map(Amount amount) {
        return new AmountDTO(amount.getId(), amount.getAmount(), amount.getUnit(), amount.getTransaction());
    }
}
