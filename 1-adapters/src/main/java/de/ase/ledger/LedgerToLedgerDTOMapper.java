package de.ase.ledger;

import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class LedgerToLedgerDTOMapper implements Function<Ledger, LedgerDTO> {
    @Override
    public LedgerDTO apply(Ledger category) {
        return map(category);
    }

    private LedgerDTO map(Ledger category) {
        return new LedgerDTO(category.getId(), category.getName(), category.getBalance());
    }
}
