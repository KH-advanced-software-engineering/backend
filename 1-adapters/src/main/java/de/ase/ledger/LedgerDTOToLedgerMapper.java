package de.ase.ledger;

import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class LedgerDTOToLedgerMapper implements Function<LedgerDTO, Ledger> {
    @Override
    public Ledger apply(LedgerDTO ledgerDTO) {
        return map(ledgerDTO);
    }

    private Ledger map(LedgerDTO ledgerDTO) {
        if (ledgerDTO == null) {
            return null;
        }
        return new Ledger(ledgerDTO.getId(), ledgerDTO.getName(), ledgerDTO.getBalance());
    }
}
