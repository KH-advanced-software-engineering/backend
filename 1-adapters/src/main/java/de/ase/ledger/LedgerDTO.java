package de.ase.ledger;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import de.ase.types.Balance;

import javax.annotation.Generated;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "balance"
})
@Generated("jsonschema2pojo")
public class LedgerDTO {
    @JsonProperty("id")
    private Long id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("balance")
    private Balance balance;


    public LedgerDTO(Long id, String name, Balance balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("description")
    public Balance getBalance() {
        return balance;
    }
}
