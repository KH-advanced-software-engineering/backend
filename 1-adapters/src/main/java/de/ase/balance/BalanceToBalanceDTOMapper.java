package de.ase.balance;

import de.ase.types.Balance;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class BalanceToBalanceDTOMapper implements Function<Balance, BalanceDTO> {
    @Override
    public BalanceDTO apply(Balance balance) {
        return map(balance);
    }

    private BalanceDTO map(Balance balance) {
        return new BalanceDTO(balance.getId(), balance.getBalance(), balance.getUnit(), balance.getLedger());
    }
}
