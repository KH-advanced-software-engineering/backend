package de.ase.balance;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import de.ase.ledger.Ledger;
import de.ase.types.Unit;

import javax.annotation.Generated;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "balance",
        "unit",
        "ledger"
})
@Generated("jsonschema2pojo")
public class BalanceDTO {
    @JsonProperty("id")
    private Long id;
    @JsonProperty("balance")
    private int balance;
    @JsonProperty("unit")
    private Unit unit;
    @JsonProperty("ledger")
    private Ledger ledger;

    public BalanceDTO(Long id, int balance, Unit unit, Ledger ledger) {
        this.id = id;
        this.balance = balance;
        this.unit = unit;
        this.ledger = ledger;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("balance")
    public int getBalance() {
        return balance;
    }

    @JsonProperty("unit")
    public Unit getUnit() {
        return unit;
    }

    @JsonProperty("ledger")
    public Ledger getLedger() {
        return ledger;
    }
}
