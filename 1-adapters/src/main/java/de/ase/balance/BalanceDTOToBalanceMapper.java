package de.ase.balance;

import de.ase.types.Balance;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class BalanceDTOToBalanceMapper implements Function<BalanceDTO, Balance> {
    @Override
    public Balance apply(BalanceDTO balanceDTO) {
        return map(balanceDTO);
    }

    private Balance map(BalanceDTO balanceDTO) {
        if (balanceDTO == null) {
            return null;
        }
        return new Balance(balanceDTO.getBalance(), balanceDTO.getUnit());
    }
}
