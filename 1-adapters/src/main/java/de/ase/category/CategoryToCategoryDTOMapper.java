package de.ase.category;

import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class CategoryToCategoryDTOMapper implements Function<Category, CategoryDTO> {
    @Override
    public CategoryDTO apply(Category category) {
        return map(category);
    }

    private CategoryDTO map(Category category) {
        return new CategoryDTO(category.getId(), category.getName(), category.getDescription());
    }
}
