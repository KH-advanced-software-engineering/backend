package de.ase.category;

import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class CategoryDTOToCategoryMapper implements Function<CategoryDTO, Category> {
    @Override
    public Category apply(CategoryDTO categoryDTO) {
        return map(categoryDTO);
    }

    private Category map(CategoryDTO categoryDTO) {
        if (categoryDTO == null) {
            return null;
        }
        return new Category(categoryDTO.getId(), categoryDTO.getName(), categoryDTO.getDescription());
    }
}
