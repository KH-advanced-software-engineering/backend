package de.ase.transaction;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import de.ase.amount.AmountDTO;
import de.ase.category.CategoryDTO;
import de.ase.ledger.LedgerDTO;
import de.ase.repeatableEvent.RepeatableEventDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.annotation.Generated;


@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "amount",
        "type",
        "category",
        "repeatableEvent",
        "ledger"
})
@Generated("jsonschema2pojo")
public class TransactionDTO {
    @JsonProperty("id")
    private Long id;
    @JsonProperty("amount")
    private AmountDTO amountDTO;
    @JsonProperty("type")
    private TransactionType type;
    @JsonProperty("category")
    private CategoryDTO categoryDTO;
    @JsonProperty("repeatableEvent")
    private RepeatableEventDTO repeatableEventDTO;
    @JsonProperty("ledger")
    private LedgerDTO ledgerDTO;


    public TransactionDTO(Long id, AmountDTO amountDTO, TransactionType type, CategoryDTO categoryDTO,
                          RepeatableEventDTO repeatableEventDTO, LedgerDTO ledgerDTO) {
        this.id = id;
        this.amountDTO = amountDTO;
        this.type = type;
        this.categoryDTO = categoryDTO;
        this.repeatableEventDTO = repeatableEventDTO;
        this.ledgerDTO = ledgerDTO;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("amount")
    public AmountDTO getAmount() {
        return amountDTO;
    }

    @JsonProperty("type")
    public TransactionType getType() {
        return type;
    }

    @JsonProperty("category")
    public CategoryDTO getCategoryDTO() {
        return categoryDTO;
    }

    @JsonProperty("repeatableEventDTO")
    public RepeatableEventDTO getRepeatableEventDTO() {
        return repeatableEventDTO;
    }

    @JsonProperty("ledgerDTO")
    public LedgerDTO getLedgerDTO() {
        return ledgerDTO;
    }
}
