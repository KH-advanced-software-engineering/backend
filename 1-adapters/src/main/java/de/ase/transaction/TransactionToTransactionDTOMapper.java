package de.ase.transaction;

import de.ase.amount.AmountToAmountDTOMapper;
import de.ase.category.CategoryToCategoryDTOMapper;
import de.ase.ledger.LedgerToLedgerDTOMapper;
import de.ase.repeatableEvent.RepeatableEventToRepeatableEventDTOMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class TransactionToTransactionDTOMapper implements Function<Transaction, TransactionDTO> {

    private AmountToAmountDTOMapper amountToAmountDTOMapper;
    private CategoryToCategoryDTOMapper categoryToCategoryDTOMapper;
    private RepeatableEventToRepeatableEventDTOMapper repeatableEventToRepeatableEventDTOMapper;
    private LedgerToLedgerDTOMapper ledgerToLedgerDTOMapper;

    @Autowired
    public TransactionToTransactionDTOMapper(AmountToAmountDTOMapper amountToAmountDTOMapper,
                                             CategoryToCategoryDTOMapper categoryToCategoryDTOMapper,
                                             RepeatableEventToRepeatableEventDTOMapper repeatableEventToRepeatableEventDTOMapper,
                                             LedgerToLedgerDTOMapper ledgerToLedgerDTOMapper) {
        this.amountToAmountDTOMapper = amountToAmountDTOMapper;
        this.categoryToCategoryDTOMapper = categoryToCategoryDTOMapper;
        this.repeatableEventToRepeatableEventDTOMapper = repeatableEventToRepeatableEventDTOMapper;
        this.ledgerToLedgerDTOMapper = ledgerToLedgerDTOMapper;
    }

    @Override
    public TransactionDTO apply(Transaction transaction) {
        return map(transaction);
    }

    private TransactionDTO map(Transaction transaction) {
        return new TransactionDTO(
                transaction.getId(),
                amountToAmountDTOMapper.apply(transaction.getAmount()),
                transaction.getType(),
                categoryToCategoryDTOMapper.apply(transaction.getCategory()),
                repeatableEventToRepeatableEventDTOMapper.apply(transaction.getRepeatableEvent()),
                ledgerToLedgerDTOMapper.apply(transaction.getLedger()));
    }
}
