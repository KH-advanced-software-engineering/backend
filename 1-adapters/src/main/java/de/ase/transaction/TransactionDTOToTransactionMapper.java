package de.ase.transaction;

import de.ase.amount.AmountDTOToAmountMapper;
import de.ase.category.CategoryDTOToCategoryMapper;
import de.ase.ledger.LedgerDTOToLedgerMapper;
import de.ase.repeatableEvent.RepeatableEventDTOToRepeatableEventMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class TransactionDTOToTransactionMapper implements Function<TransactionDTO, Transaction> {

    private AmountDTOToAmountMapper amountDTOToAmountMapper;
    private CategoryDTOToCategoryMapper categoryDTOToCategoryMapper;
    private RepeatableEventDTOToRepeatableEventMapper repeatableEventDTOToRepeatableEventMapper;
    private LedgerDTOToLedgerMapper ledgerDTOToLedgerMapper;

    @Autowired
    public TransactionDTOToTransactionMapper(AmountDTOToAmountMapper amountDTOToAmountMapper,
                                             CategoryDTOToCategoryMapper categoryDTOToCategoryMapper,
                                             RepeatableEventDTOToRepeatableEventMapper repeatableEventDTOToRepeatableEventMapper,
                                             LedgerDTOToLedgerMapper ledgerDTOToLedgerMapper) {
        this.amountDTOToAmountMapper = amountDTOToAmountMapper;
        this.categoryDTOToCategoryMapper = categoryDTOToCategoryMapper;
        this.repeatableEventDTOToRepeatableEventMapper = repeatableEventDTOToRepeatableEventMapper;
        this.ledgerDTOToLedgerMapper = ledgerDTOToLedgerMapper;
    }

    @Override
    public Transaction apply(TransactionDTO transactionDTO) {
        return map(transactionDTO);
    }

    private Transaction map(TransactionDTO transactionDTO) {
        if (transactionDTO == null) {
            return null;
        }
        return new Transaction(
                transactionDTO.getId(),
                amountDTOToAmountMapper.apply(transactionDTO.getAmount()),
                transactionDTO.getType(),
                categoryDTOToCategoryMapper.apply(transactionDTO.getCategoryDTO()),
                repeatableEventDTOToRepeatableEventMapper.apply(transactionDTO.getRepeatableEventDTO()),
                ledgerDTOToLedgerMapper.apply(transactionDTO.getLedgerDTO()));
    }
}
