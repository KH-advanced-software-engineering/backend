package de.ase.ledger;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import de.ase.transaction.Transaction;
import de.ase.types.Balance;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name = "ledger")
@Entity
public class Ledger {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name")
    private String name;
    @OneToOne
    @JsonIgnoreProperties("ledger")
    @JoinColumn(name = "balance_id")
    private Balance balance;
    @Transient
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ledger")
    private List<Transaction> transactions;

    public Ledger() {
    }

    public Ledger(Long id, String name, Balance balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
        this.transactions = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Balance getBalance() {
        return balance;
    }

    public void setBalance(Balance balance) {
        this.balance = balance;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }
}