package de.ase.ledger;

public interface LedgerRepository {
    void save(Ledger ledger);

    Ledger getById(Long id);

    void delete(Ledger ledger);
}
