package de.ase.types;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import de.ase.ledger.Ledger;

import javax.persistence.*;

@Table(name = "balance")
@Entity
public final class Balance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "balance")
    private int balance;
    @Column(name = "unit")
    private Unit unit;
    @OneToOne(mappedBy = "balance")
    @JsonIgnoreProperties("balance")
    private Ledger ledger;

    public Balance() {
    }

    public Balance(int balance, Unit unit) {
        this.balance = balance;
        this.unit = unit;
    }

    public Balance(int balance, Unit unit, Ledger ledger) {
        this.balance = balance;
        this.unit = unit;
        this.ledger = ledger;
    }

    public int getBalance() {
        return balance;
    }

    public Unit getUnit() {
        return unit;
    }

    public Long getId() {
        return id;
    }

    public Ledger getLedger() {
        return ledger;
    }
}