package de.ase.types;

public enum Unit {
    DOLLAR("$"), EURO("€");

    private final String unit;

    private Unit(String unit) {
        this.unit = unit;
    }

    public String getUnit() {
        return unit;
    }
}
