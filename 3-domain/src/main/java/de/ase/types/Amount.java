package de.ase.types;

import de.ase.transaction.Transaction;

import javax.persistence.*;

@Entity
@Table(name = "amount")
public final class Amount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "amount")
    private int amount;
    @Column(name = "unit")
    private Unit unit;
    @OneToOne(mappedBy = "amount")
    private Transaction transaction;

    public Amount() {
    }

    public Amount(int amount, Unit unit) {
        if (amount < 0) {
            throw new IllegalArgumentException("The amount should be greater or equals zero!");
        }
        this.amount = amount;
        this.unit = unit;
    }

    public int getAmount() {
        return amount;
    }

    public Unit getUnit() {
        return unit;
    }

    public Long getId() {
        return id;
    }

    public Transaction getTransaction() {
        return transaction;
    }
}
