package de.ase.category;


import java.util.List;

public interface CategoryRepository {

    void save(Category category);

    Category getByName(String name);

    List<Category> findAll();

    void delete(Category category);
}
