package de.ase.balanceCalculation;

import de.ase.ledger.Ledger;
import de.ase.types.Amount;
import de.ase.types.Balance;

public class SumLedgerBalanceCalculationStrategy implements LedgerBalanceCalculationStrategy {
    @Override
    public void calculateNewBalanceForLeger(Ledger ledger, Amount amount) {
        Balance oldBalance = ledger.getBalance();
        int balance = oldBalance.getBalance() + amount.getAmount();
        if (oldBalance.getUnit() != amount.getUnit()) {
            throw new IllegalArgumentException("The unit of the balance and the amount should be the same.");
        }
        ledger.setBalance(new Balance(balance, oldBalance.getUnit()));
    }
}
