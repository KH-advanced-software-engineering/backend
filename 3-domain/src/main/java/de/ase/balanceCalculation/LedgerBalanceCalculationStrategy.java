package de.ase.balanceCalculation;

import de.ase.ledger.Ledger;
import de.ase.types.Amount;

public interface LedgerBalanceCalculationStrategy {
    void calculateNewBalanceForLeger(Ledger ledger, Amount amount);
}
