package de.ase.transaction;

public enum TransactionType {
    INCOME, SPENDING
}
