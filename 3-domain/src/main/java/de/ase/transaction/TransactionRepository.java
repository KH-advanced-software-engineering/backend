package de.ase.transaction;

import de.ase.ledger.Ledger;

import java.time.LocalDate;
import java.util.List;

public interface TransactionRepository {
    boolean existsById(Long transactionId);

    void save(Transaction transaction);

    Transaction getById(Long transactionId);

    List<Transaction> getTransactionsInTimeInterval(LocalDate startDate, LocalDate endDate);

    List<Transaction> findAllByLedger(Ledger ledger);

    void deleteById(Long transactionId);
}
