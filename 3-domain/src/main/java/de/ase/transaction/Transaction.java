package de.ase.transaction;

import de.ase.category.Category;
import de.ase.ledger.Ledger;
import de.ase.repeatableEvent.RepeatableEvent;
import de.ase.types.Amount;

import javax.persistence.*;

@Table(name = "transaction")
@Entity
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne
    @JoinColumn(name = "amount_id")
    private Amount amount;
    @Column(name = "type")
    private TransactionType type;
    @OneToOne
    @JoinColumn(name = "category_id")
    private Category category;
    @OneToOne
    @JoinColumn(name = "repeatable_id")
    private RepeatableEvent repeatableEvent;
    @ManyToOne
    @JoinColumn(name = "ledger_id", nullable = false)
    private Ledger ledger;

    public Transaction() {
    }

    public Transaction(Long id, Amount amount, TransactionType type, Category category,
                       RepeatableEvent repeatableEvent, Ledger ledger) {
        this.id = id;
        this.amount = amount;
        this.type = type;
        this.category = category;
        this.repeatableEvent = repeatableEvent;
    }

    public Amount getAmount() {
        return amount;
    }

    public void setAmount(Amount amount) {
        this.amount = amount;
    }

    public TransactionType getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public RepeatableEvent getRepeatableEvent() {
        return repeatableEvent;
    }

    public void setRepeatableEvent(RepeatableEvent repeatableEvent) {
        this.repeatableEvent = repeatableEvent;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Ledger getLedger() {
        return ledger;
    }

    public void setLedger(Ledger ledger) {
        this.ledger = ledger;
    }
}
