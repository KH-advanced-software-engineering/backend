package de.ase.repeatableEvent;

import com.fasterxml.jackson.annotation.JsonFormat;
import de.ase.transaction.Transaction;

import javax.persistence.*;
import java.time.LocalDate;

@Table(name = "repeatableEvent")
@Entity
public class RepeatableEvent {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "startDate")
    @JsonFormat(pattern = "MM/dd/yyyy")
    private LocalDate startDate;
    @Column(name = "endDate")
    @JsonFormat(pattern = "MM/dd/yyyy")
    private LocalDate endDate;
    @Column(name = "interval")
    private int interval;
    @OneToOne(mappedBy = "repeatableEvent")
    private Transaction transaction;

    public RepeatableEvent() {

    }

    public RepeatableEvent(LocalDate startDate, LocalDate endDate, int interval) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.interval = interval;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }
}
