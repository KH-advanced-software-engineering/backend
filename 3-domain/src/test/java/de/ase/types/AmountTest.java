package de.ase.types;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AmountTest {

    @Test
    public void testValidConstruction() {
        Amount amount = new Amount(1, Unit.EURO);
        assertEquals(1, amount.getAmount());
        assertEquals(Unit.EURO, amount.getUnit());

        Amount amount1 = new Amount(12787, Unit.DOLLAR);
        assertEquals(12787, amount1.getAmount());
        assertEquals(Unit.DOLLAR, amount1.getUnit());
    }

    @Test
    void negativeAmountThrowsException() {
        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> new Amount(-1, Unit.EURO),
                "Expected Amount to throw, but it didn't"
        );
        assertTrue(thrown.getMessage().contains("The amount should be greater or equals zero!"));
    }
}