package de.ase.persistence.hibernate.ledger;

import de.ase.ledger.Ledger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface SpringDataLedgerRepository extends JpaRepository<Ledger, Long> {
    Ledger getById(Long id);
}