package de.ase.persistence.hibernate.transaction;

import de.ase.ledger.Ledger;
import de.ase.transaction.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface SpringDataTransactionRepository extends JpaRepository<Transaction, Long> {
    Transaction getById(Long id);

    List<Transaction> findAllByLedger(Ledger ledger);
}