package de.ase.persistence.hibernate.ledger;

import de.ase.ledger.Ledger;
import de.ase.ledger.LedgerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class LedgerRepositoryBridge implements LedgerRepository {
    private SpringDataLedgerRepository springDataLedgerRepository;

    @Autowired
    public LedgerRepositoryBridge(SpringDataLedgerRepository springDataLedgerRepository) {
        this.springDataLedgerRepository = springDataLedgerRepository;
    }

    @Override
    public void save(Ledger ledger) {
        this.springDataLedgerRepository.save(ledger);
    }

    @Override
    public Ledger getById(Long id) {
        return springDataLedgerRepository.getById(id);
    }

    @Override
    public void delete(Ledger ledger) {
        springDataLedgerRepository.delete(ledger);
    }
}



