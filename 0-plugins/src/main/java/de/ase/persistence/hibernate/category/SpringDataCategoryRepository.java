package de.ase.persistence.hibernate.category;

import de.ase.category.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface SpringDataCategoryRepository extends JpaRepository<Category, Long> {
    Category getByName(String name);
}