package de.ase.persistence.hibernate.category;

import de.ase.category.Category;
import de.ase.category.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CategoryRepositoryBridge implements CategoryRepository {
    private SpringDataCategoryRepository springDataCategoryRepository;

    @Autowired
    public CategoryRepositoryBridge(SpringDataCategoryRepository springDataCategoryRepository) {
        this.springDataCategoryRepository = springDataCategoryRepository;
    }

    @Override
    public void save(Category category) {
        this.springDataCategoryRepository.save(category);
    }

    @Override
    public Category getByName(String name) {
        return springDataCategoryRepository.getByName(name);
    }

    @Override
    public List<Category> findAll() {
        return springDataCategoryRepository.findAll();
    }

    @Override
    public void delete(Category category) {
        springDataCategoryRepository.delete(category);
    }
}



