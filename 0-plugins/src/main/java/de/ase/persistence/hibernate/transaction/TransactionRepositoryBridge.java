package de.ase.persistence.hibernate.transaction;

import de.ase.ledger.Ledger;
import de.ase.transaction.Transaction;
import de.ase.transaction.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public class TransactionRepositoryBridge implements TransactionRepository {
    private SpringDataTransactionRepository springDataTransactionRepository;

    @Autowired
    public TransactionRepositoryBridge(SpringDataTransactionRepository springDataTransactionRepository) {
        this.springDataTransactionRepository = springDataTransactionRepository;
    }

    @Override
    public void save(Transaction transaction) {
        springDataTransactionRepository.save(transaction);
    }

    @Override
    public Transaction getById(Long id) {
        return springDataTransactionRepository.getById(id);
    }

    @Override
    public boolean existsById(Long transactionId) {
        return false;
    }

    @Override
    public List<Transaction> getTransactionsInTimeInterval(LocalDate startDate, LocalDate endDate) {
        return null;
    }

    @Override
    public List<Transaction> findAllByLedger(Ledger ledger) {
        return springDataTransactionRepository.findAllByLedger(ledger);
    }

    @Override
    public void deleteById(Long transactionId) {
        springDataTransactionRepository.deleteById(transactionId);
    }
}



