package de.ase.rest.ledger;

import de.ase.balance.BalanceDTO;
import de.ase.balance.BalanceDTOToBalanceMapper;
import de.ase.ledger.LedgerApplicationService;
import de.ase.ledger.LedgerDTO;
import de.ase.ledger.LedgerDTOToLedgerMapper;
import de.ase.ledger.LedgerToLedgerDTOMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/ledger")
public class LedgerController {

    private final LedgerApplicationService ledgerApplicationService;
    private final LedgerDTOToLedgerMapper ledgerDTOToLedgerMapper;
    private final LedgerToLedgerDTOMapper ledgerToLedgerDTOMapper;
    private BalanceDTOToBalanceMapper balanceDTOToBalanceMapper;

    @Autowired
    public LedgerController(LedgerApplicationService ledgerApplicationService,
                            LedgerDTOToLedgerMapper ledgerDTOToLedgerMapper,
                            LedgerToLedgerDTOMapper ledgerToLedgerDTOMapper,
                            BalanceDTOToBalanceMapper balanceDTOToBalanceMapper) {
        this.ledgerApplicationService = ledgerApplicationService;
        this.ledgerDTOToLedgerMapper = ledgerDTOToLedgerMapper;
        this.ledgerToLedgerDTOMapper = ledgerToLedgerDTOMapper;
        this.balanceDTOToBalanceMapper = balanceDTOToBalanceMapper;
    }

    @PostMapping(value = "/create")
    public void createLedger(@RequestBody LedgerDTO ledgerDTO) throws IllegalArgumentException {
        this.ledgerApplicationService.createLedger(this.ledgerDTOToLedgerMapper.apply(ledgerDTO));
    }

    @GetMapping(value = "/readById", params = {"id"})
    public LedgerDTO getLedgerById(@RequestParam Long id) throws IllegalArgumentException {
        return this.ledgerToLedgerDTOMapper.apply(this.ledgerApplicationService.getLedgerById(id));
    }

    @PutMapping(value = "/update", params = {"name", "balance"})
    public void updateLedger(@RequestBody LedgerDTO ledgerDTO, @RequestParam String name,
                             @RequestParam BalanceDTO balanceDTO) throws
            IllegalArgumentException {
        this.ledgerApplicationService.updateLedger(this.ledgerDTOToLedgerMapper.apply(ledgerDTO), name,
                balanceDTOToBalanceMapper.apply(balanceDTO), null);
    }

    @DeleteMapping("/delete")
    public void deleteLedger(@RequestParam LedgerDTO ledgerDTO) throws IllegalArgumentException {
        this.ledgerApplicationService.deleteLedger(this.ledgerDTOToLedgerMapper.apply(ledgerDTO));
    }
}

