package de.ase.rest.category;

import de.ase.category.CategoryApplicationService;
import de.ase.category.CategoryDTO;
import de.ase.category.CategoryDTOToCategoryMapper;
import de.ase.category.CategoryToCategoryDTOMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/category")
public class CategoryController {

    private final CategoryApplicationService categoryApplicationService;
    private final CategoryDTOToCategoryMapper categoryDTOToCategoryMapper;
    private final CategoryToCategoryDTOMapper categoryToCategoryDTOMapper;

    @Autowired
    public CategoryController(CategoryApplicationService categoryApplicationService,
                              CategoryDTOToCategoryMapper categoryDTOToCategoryMapper,
                              CategoryToCategoryDTOMapper categoryToCategoryDTOMapper) {
        this.categoryApplicationService = categoryApplicationService;
        this.categoryDTOToCategoryMapper = categoryDTOToCategoryMapper;
        this.categoryToCategoryDTOMapper = categoryToCategoryDTOMapper;
    }

    @PostMapping(value = "/create")
    public void createCategory(@RequestBody CategoryDTO categoryDTO) throws IllegalArgumentException {
        this.categoryApplicationService.createCategory(this.categoryDTOToCategoryMapper.apply(categoryDTO));
    }

    @GetMapping(value = "/readByName", params = {"name"})
    public CategoryDTO getCategoryByName(@RequestParam String name) throws IllegalArgumentException {
        return this.categoryToCategoryDTOMapper.apply(this.categoryApplicationService.getCategoryByName(name));
    }

    @GetMapping(value = "/read")
    public List<CategoryDTO> getAllCategories() {
        return this.categoryApplicationService.getAllCategories()
                .stream()
                .map(this.categoryToCategoryDTOMapper::apply)
                .collect(Collectors.toList());
    }

    @PutMapping(value = "/update", params = {"name", "description"})
    public void updateCategory(@RequestBody CategoryDTO categoryDTO, @RequestParam String name,
                               @RequestParam String description) throws
            IllegalArgumentException {
        this.categoryApplicationService
                .updateCategory(categoryDTOToCategoryMapper.apply(categoryDTO), name, description);
    }

    @DeleteMapping("/delete")
    public void deleteCategory(@RequestBody CategoryDTO categoryDTO) throws IllegalArgumentException {
        this.categoryApplicationService.deleteCategory(this.categoryDTOToCategoryMapper.apply(categoryDTO));
    }
}

