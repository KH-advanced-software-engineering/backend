package de.ase.rest.transaction;

import de.ase.transaction.*;
import de.ase.balanceCalculation.LedgerBalanceCalculationStrategy;
import de.ase.balanceCalculation.SubtractionLedgerBalanceCalculationStrategy;
import de.ase.balanceCalculation.SumLedgerBalanceCalculationStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/transaction")
public class TransactionController {

    private TransactionApplicationService transactionApplicationService;
    private TransactionDTOToTransactionMapper transactionDTOToTransactionMapper;
    private TransactionToTransactionDTOMapper transactionToTransactionDTOMapper;

    @Autowired
    public TransactionController(TransactionApplicationService transactionApplicationService,
                                 TransactionDTOToTransactionMapper transactionDTOToTransactionMapper,
                                 TransactionToTransactionDTOMapper transactionToTransactionDTOMapper) {
        this.transactionApplicationService = transactionApplicationService;
        this.transactionDTOToTransactionMapper = transactionDTOToTransactionMapper;
        this.transactionToTransactionDTOMapper = transactionToTransactionDTOMapper;
    }

    @PostMapping(value = "/create")
    public void createLedger(@RequestBody TransactionDTO transactionDTO) throws IllegalArgumentException,
            ParseException {
        Transaction transaction = this.transactionDTOToTransactionMapper.apply(transactionDTO);
        LedgerBalanceCalculationStrategy strategy = new SumLedgerBalanceCalculationStrategy();
        if (transaction.getType() != null) {
            if (transaction.getType() == TransactionType.SPENDING) {
                strategy = new SubtractionLedgerBalanceCalculationStrategy();
            }
        } else {
            throw new IllegalArgumentException("A new transaction needs a transaction type.");
        }
        this.transactionApplicationService
                .createTransaction(transaction, strategy);
    }

    @GetMapping(value = "/readById", params = {"id"})
    public TransactionDTO getTransactionById(@RequestParam Long id) throws IllegalArgumentException {
        return this.transactionToTransactionDTOMapper.apply(this.transactionApplicationService.getTransactionById(id));
    }

    @GetMapping(value = "/read")
    public List<TransactionDTO> getAllTransactionsForLedger(@RequestBody TransactionDTO transactionDTO) {
        return this.transactionApplicationService
                .getAllTransactionsForLedger(transactionDTOToTransactionMapper.apply(transactionDTO).getLedger())
                .stream()
                .map(this.transactionToTransactionDTOMapper::apply)
                .collect(Collectors.toList());
    }

    @PutMapping(value = "/update")
    public void updateTransaction(@RequestBody TransactionDTO transactionDTO) throws
            IllegalArgumentException {
        Transaction transaction = transactionDTOToTransactionMapper.apply(transactionDTO);
        this.transactionApplicationService
                .updateTransaction(transaction.getId(), transaction.getAmount(), transaction.getCategory(),
                        transaction.getType(), transaction.getRepeatableEvent());
    }

    @DeleteMapping("/delete")
    public void deleteTransaction(@RequestParam TransactionDTO transactionDTO) throws IllegalArgumentException {
        this.transactionApplicationService
                .deleteTransaction(this.transactionDTOToTransactionMapper.apply(transactionDTO).getId());
    }
}

