package de.ase.rest.transaction;

import de.ase.ledger.Ledger;
import de.ase.ledger.LedgerDTO;
import de.ase.ledger.LedgerDTOToLedgerMapper;
import de.ase.transaction.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/transaction/filter")
public class TransactionFilterController {

    private TransactionFilterApplicationService transactionFilterApplicationService;
    private TransactionDTOToTransactionMapper transactionDTOToTransactionMapper;
    private TransactionToTransactionDTOMapper transactionToTransactionDTOMapper;
    private LedgerDTOToLedgerMapper ledgerDTOToLedgerMapper;

    @Autowired
    public TransactionFilterController(TransactionFilterApplicationService transactionFilterApplicationService,
                                       TransactionDTOToTransactionMapper transactionDTOToTransactionMapper,
                                       TransactionToTransactionDTOMapper transactionToTransactionDTOMapper,
                                       LedgerDTOToLedgerMapper ledgerDTOToLedgerMapper) {
        this.transactionFilterApplicationService = transactionFilterApplicationService;
        this.transactionDTOToTransactionMapper = transactionDTOToTransactionMapper;
        this.transactionToTransactionDTOMapper = transactionToTransactionDTOMapper;
        this.ledgerDTOToLedgerMapper = ledgerDTOToLedgerMapper;
    }

    @GetMapping(value = "/isDuplicate")
    public boolean isTransactionADuplicate(@RequestParam TransactionDTO transactionDTO) throws
            IllegalArgumentException {
        Transaction transaction = transactionDTOToTransactionMapper.apply(transactionDTO);
        return this.transactionFilterApplicationService
                .isTransactionADuplicate(transaction, transaction.getLedger());
    }

    @GetMapping(value = "/readInterval", params = {"startDate", "endDate"})
    public List<TransactionDTO> getAllTransactionsInInterval(@RequestBody LedgerDTO ledgerDTO, @RequestParam
            LocalDate startDate, @RequestParam LocalDate endDate) {
        Ledger ledger = ledgerDTOToLedgerMapper.apply(ledgerDTO);
        return this.transactionFilterApplicationService
                .getAllTransactionsInInterval(startDate, endDate, ledger)
                .stream()
                .map(this.transactionToTransactionDTOMapper::apply)
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/readIntervalByType", params = {"startDate", "endDate", "type"})
    public List<TransactionDTO> getTransactionsInIntervalByType(@RequestBody LedgerDTO ledgerDTO, @RequestParam
            LocalDate startDate, @RequestParam LocalDate endDate, @RequestParam TransactionType type) {
        Ledger ledger = ledgerDTOToLedgerMapper.apply(ledgerDTO);
        return this.transactionFilterApplicationService
                .getTransactionsInIntervalByType(startDate, endDate, ledger, type)
                .stream()
                .map(this.transactionToTransactionDTOMapper::apply)
                .collect(Collectors.toList());
    }
}

